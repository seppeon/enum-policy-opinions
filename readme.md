Given the following month class:
```C++
enum class Month
{
    January = 1,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December,
};
```

And the following weekday class:
```C++
enum class Weekday
{
    Sunday,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday
};
```
*Code 0*

With the ToEnum conversion functions:
```C++
#include <optional>
/////////////////////////////////////////////////////////////////////
/**
 * No Month should ever be produced should be invalid.
 */
std::optional<Month> ToMonth(int value)
{
    constexpr Month lut[]{
        Month::January,
        Month::February,
        Month::March,
        Month::April,
        Month::May,
        Month::June,
        Month::July,
        Month::August,
        Month::September,
        Month::October,
        Month::November,
        Month::December,
    };
    for (auto v : lut) if (static_cast<int>(v) == value) return v;
    return {};
}
/**
 * No Day should ever be produced should be invalid.
 */
std::optional<Weekday> ToWeekday(int value)
{
    constexpr Weekday lut[]{
        Weekday::Monday,
        Weekday::Tuesday,
        Weekday::Wednesday,
        Weekday::Thursday,
        Weekday::Friday,
        Weekday::Saturday,
        Weekday::Sunday,
    };
    for (auto v : lut) if (static_cast<int>(v) == value) return v;
    return {};
}
```
*Code 1*

And the following ToUnderlyingType conversion functions:
```C++
/**
 * No Month should ever be produced should be invalid.
 */
std::optional<int> ToInt(Month value)
{
    if (Month::January <= value and value <= Month::December) return static_cast<int>(value);
    else return {};
}
/**
 * No Day should ever be produced should be invalid.
 */
std::optional<int> ToInt(Weekday value)
{
    if (Weekday::Monday <= value and value <= Weekday::Sunday) return static_cast<int>(value);
    else return {};
}
/////////////////////////////////////////////////////////////////////
```
*Code 2*

# Policy
 "An enum must have a nameable value."

# Precedence
## Pointers
- Pointers have a set of valid values, just like enums.
- A set of invalid values, just like enums.
- Pointers are convertible to and from an integer type, just like enums.
- Pointers have a null state, a labelled value, just like enums.
- References are essentially pointers, they are never check for nullptr.
     
However, if we have an address of a pointer (uintptr_t or intptr_t) we don't check it against all valid memory ranges. Why, because the checks arn't scalable, add a new memory region and the check creates a bug (the error is in the error detection).

# Bug spawning
- If the ToInt function is not updated with the enum, or incorrect ranges are checked then you create bugs, not prevent them.
- [opinion] Through maintainance cycles people are  more prone to sorting and reordering enum fields than they are to cast random integers to enums (ie. alphabetical, different first days of the week etc... this breaks range based checks).

# Mindset
[opinion] The mindset that bugs will be caught down the line is unacceptable. Code should be tested to not produce invalid values.

# Detectability
C++'s `enum class` prevents the classic implicit conversions of int to enums:
```C++
// Old style enum
enum Foo
{
    Red
};
Foo foo = 10; // compiles

// New style enum
enum class Bar
{
	Red
};
Bar bar = 10; // a compiler error
``` 

In the case of casts:
1. Clang-Tidy can already flag `reinterpret_cast` casts with [cppcoreguidelines-pro-type-reinterpret-cast](https://clang.llvm.org/extra/clang-tidy/checks/cppcoreguidelines-pro-type-reinterpret-cast.html).
2. Clang-Tidy can already flag C-Style casts with [cppcoreguidelines-pro-type-cstyle-cast](https://clang.llvm.org/extra/clang-tidy/checks/cppcoreguidelines-pro-type-cstyle-cast.html)
3. [Checkers can be made for](https://blog.audio-tk.com/2018/03/20/writing-custom-checks-for-clang-tidy/) `static_cast` of enums, flagging these sites for review.
4. Git merge requests can grep for static_cast and reinterpret_cast's. This is good policy even ignoring the issue of enums.

# Confusion Inconsistancy
If code checks enum preconditions, do nested functions check it also?

# Binary Size Bloat
The checks required produce various braches or contracts both consuming more memory.

# Performance
Branches produce one of the larger perf hits for systems.

# Maintainance
Maintaining code which has these sorts of checks results in code like this:

```C++
void SetDate(WeekDay day, Month month)
{
    if ((month >= Month::January and month < Month::December) and 
    	(week_day >= WeekDay::Monday and week_day < WeekDay::Sunday))
    {
    	DoThingsWithValidDate(day, month);
    }
    InvalidDate();
}
```
*Code 3*

Which if the enum is reordered will fail rarely enough to pass tests, often enough to be a problem.

Or when using the code from *Code 2*: 
```C++
void SetDate(WeekDay day, Month month)
{
    if (auto month = ToInt(Month::January))
    {
        if (auto day = ToInt(Day::Monday))
        {
    		DoThingsWithValidDate(day, month);
        }
        else
        {
    		InvalidMonth();
        }
    }
    else
    {
    	InvalidDay();
    }
}
```
*Code 4*

These blocks of code have relatively high complexity when compared to code which is only allowed to produce valid enum values. These are relatively simple cases, for a peripheral such as USART the complexity is substantially higher due to the high number of configuration options required at initalization.

# Redundant and Complexity
If all enums produced are valid then all checks of enums are redundant. If checks are performed, redundant checks are performed in nested functions.

If the value `month` of type Month must be validated:
```C++
bool DoStuff(Month month)
{
    if (not PrintDate(month)) return false; // checks internally, month cannot be trusted.
    if (not StoreDate(month)) return false; // checks internally, month cannot be trusted.
    return true;
}
```
This has 2 checks 3 paths.
```C++
if (DoStuff(untrusted_month))
{
}
else
{
	// Error
}
```
This has 1 check, 2 paths.

Totals 3 checks, 5 paths.
___
Instead of a much more readable:
```C++
void DoStuff(Month month)
{
    PrintDate(month);
    StoreDate(month);
}
```
This has 0 checks, 1 path.
```C++
if (auto month = ToMonth(untrusted_month))
{
    DoStuff(month.value());
}
else
{
	// Error.
}
```
This has 1 check, 2 paths.

Totals 1 check, 3 paths.
___
**The version which cannot trust an `enum class` has higher path count, and higher checks but achieves nothing extra**

# Readability
The more code, the more sites that errors can exist. 

Did you spot these errors in the post?

In *Code 2*, erroring on the range of the weekdays (The enum is Sunday to Saturday):
```C++
if (Weekday::Monday <= value and value <= Weekday::Sunday) return static_cast<int>(value);
```
In *Code 3*, erroring on the range of the weekdays (The enum is Sunday to Saturday):
```C++
(week_day >= WeekDay::Monday and week_day < WeekDay::Sunday)
```
In *Code 4*, erroring on day instead of month:
```C++
if (auto month = ToInt(Month::January))
{
	...
}
else
{
	InvalidDay();
}
```
In *Code 4*, erroring with month instead of day:
```CPP
if (auto day = ToInt(Day::Monday))
{
}
else
{
	InvalidMonth();
}
```

# Trivial Setters
Lets look at an implementation of SetMonth which validates `month`:
```CPP
struct Date
{
    bool SetMonth(Month month)
    {
        if (auto v = ToInt(month))
    	{
            m_month = v.value();
            return true;
        }
        else
        {
            return false;
    	}
    }
private:
    Month m_month = Month::January;
};
```
If this was enforced **consistantly** it leads to trivial Setter functions requiring boolean returns. Even setters for ordinary variables.

# Conclusion
All enum (`Enum`) **source** functions are **required** to produce valid values with the exception of those producing `std::byte` (sometimes implemented in terms of enum class). A producing function which can fail will return a `std::optional<Enum>`(a noexcept version of `std::optional`).

An enum of type `Enum` is trusted. Functions which **sink** enums will trust input, as all functions which produced the enum have produced valid values (or invalid, checkable via the result from a `std::optional<Enum>`.

All enum's will be defined as `enum class`. This prevents implicit conversions from the underlying type to the enum.

Casts of an underling type to an enum are expressly forbidden in non-hardware level code. Casts will be flagged automatically for review and static analysis will check files for `static_cast`/`reinterpret_cast` **to** enum's. The former, flagging system is simple to implement so should be produced first. The latter, harder to implement will detect casts that are missed by reviewers.
